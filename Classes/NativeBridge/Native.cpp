//
//  Native.cpp
//  Furyu
//
//  Created by Nguyen Khac Uy on 1/22/16.
//
//

#include "Native.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <jni.h>
#include <android/log.h>
#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NativeIOS.h"
#endif

void Native::closeCocos()
{
    NativeIOS::closeViewCocos();
}