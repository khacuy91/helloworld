//
//  NativeIOS.mm
//  Furyu
//
//  Created by Nguyen Khac Uy on 1/22/16.
//
//

#include "NativeIOS.h"
#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "ViewController.h"

void NativeIOS::closeViewCocos()
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}