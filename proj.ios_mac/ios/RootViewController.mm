/****************************************************************************
 Copyright (c) 2013      cocos2d-x.org
 Copyright (c) 2013-2014 Chukong Technologies Inc.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "RootViewController.h"
#import "cocos2d.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "AppDelegate.h"

@implementation RootViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(void)loadOpenglScene:(CGRect)rect {
    cocos2d::Application *app = cocos2d::Application::getInstance();
    app->initGLContextAttrs();
    cocos2d::GLViewImpl::convertAttrs();
    
    // Override point for customization after application launch.
    
    // Add the view controller's view to the window and display.
    //window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Init the CCEAGLView
    CCEAGLView *eaglView = [CCEAGLView viewWithFrame: [[UIScreen mainScreen] bounds]
                                         pixelFormat: (NSString*)cocos2d::GLViewImpl::_pixelFormat
                                         depthFormat: cocos2d::GLViewImpl::_depthFormat
                                  preserveBackbuffer: NO
                                          sharegroup: nil
                                       multiSampling: NO
                                     numberOfSamples: 0 ];
    
    // Enable or disable multiple touches
    [eaglView setMultipleTouchEnabled:NO];
    
    [self.view addSubview:eaglView];
    
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    
    app->run();
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    cocos2d::Director *director = cocos2d::Director::getInstance();
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGRect calculatedRect = CGRectMake(0, 0, screen.size.width, screen.size.height);
    
    // If we have not created anf atached the EAGLView then do that now.
    if (director->getOpenGLView() == NULL) {
        //
        // Create the EAGLView manually
        //  1. Create a RGB565 format. Alternative: RGBA8
        //  2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
        //
        //
        
        //        EAGLView *glView = [EAGLView viewWithFrame:calculatedRect
        //                                       pixelFormat:kEAGLColorFormatRGB565   // kEAGLColorFormatRGBA8
        //                                       depthFormat:0                        // GL_DEPTH_COMPONENT16_OES
        //                            ];
        cocos2d::Application *app = cocos2d::Application::getInstance();
        app->initGLContextAttrs();
        cocos2d::GLViewImpl::convertAttrs();
        
        CCEAGLView *eaglView = [CCEAGLView viewWithFrame: calculatedRect
                                             pixelFormat: (NSString*)cocos2d::GLViewImpl::_pixelFormat
                                             depthFormat: cocos2d::GLViewImpl::_depthFormat
                                      preserveBackbuffer: NO
                                              sharegroup: nil
                                           multiSampling: NO
                                         numberOfSamples: 0 ];
        
        // Enable or disable multiple touches
        [eaglView setMultipleTouchEnabled:NO];
        // viewWithFrame:calculatedRect pixelFormat:kEAGLColorFormatRGB565 depthFormat:0];
        //
        //        // attach the openglView to the director
        cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglView);
        cocos2d::Director::getInstance()->setOpenGLView(glview);
        app->run();
    } else {
        CCEAGLView *eaglView = (CCEAGLView*)director->getOpenGLView()->getEAGLView();
        //        [self.view addSubview:eaglView];
        [eaglView removeFromSuperview];
        if (!CGRectEqualToRect(eaglView.bounds, calculatedRect)) {
            [eaglView setFrame:calculatedRect];
        }
    }
    CCEAGLView *eaglView = (CCEAGLView*)director->getOpenGLView()->getEAGLView();
    [self.view addSubview:eaglView];
    //    [director setDeviceOrientation:UIDeviceOrientationPortrait];
    
    director->setAnimationInterval(1.0/60);
}


// Override to allow orientations other than the default portrait orientation.
// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}

// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations{
#ifdef __IPHONE_6_0
    return UIInterfaceOrientationMaskAllButUpsideDown;
#endif
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    auto glview = cocos2d::Director::getInstance()->getOpenGLView();

    if (glview)
    {
        CCEAGLView *eaglview = (CCEAGLView*) glview->getEAGLView();

        if (eaglview)
        {
            CGSize s = CGSizeMake([eaglview getWidth], [eaglview getHeight]);
            cocos2d::Application::getInstance()->applicationScreenSizeChanged((int) s.width, (int) s.height);
        }
    }
}

//fix not hide status on ios7
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewDidDisappear: (BOOL) animated {
    cocos2d::Director::getInstance()->end();
}

- (void)dealloc {
    [super dealloc];
}

-(void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
