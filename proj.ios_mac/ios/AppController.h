#import <UIKit/UIKit.h>

@class ViewController;

@interface AppController : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}

@property (strong, nonatomic) UIWindow *window;

@end

