//
//  ViewController.m
//  Menu Apps
//
//  Created by Nguyen Khac Uy on 2/16/16.
//  Copyright © 2016 nal. All rights reserved.
//

#import "ViewController.h"
#import "RootViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//fix not hide status on ios7
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)openFuryu:(id)sender {
    RootViewController *viewController = [[RootViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:NULL];
}

-(void)viewDidDisappear: (BOOL) animated {
    
}

@end
